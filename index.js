const express = require('express');
const bodyParser = require('body-parser');

const app = express();

// parse application/json
app.use(bodyParser.json());

//variables
var variables = {
    lampara: {
        on: false
    },
    monitor: {
        on: false
    }
};

app.get('/', function(req, res){
    res.json({
        name: 'webserver_david',
        version: '1.0'
    });
});

app.get('/variables', function(req, res){
    res.json(variables);
});

app.post('/variables', function(req, res){

    if( typeof req.body.lampara != 'undefined' ){
        variables.lampara.on = req.body.lampara == true || req.body.lampara == 'true';
    }

    if( typeof req.body.monitor != 'undefined' ){
        variables.monitor.on = req.body.monitor == true || req.body.monitor == 'true';
    }

    res.json(variables);
});

app.listen(3001, () => {
    console.log(`Escuchando el puerto 80 ...`); 
});
